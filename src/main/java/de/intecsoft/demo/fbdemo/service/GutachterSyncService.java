package de.intecsoft.demo.fbdemo.service;

import de.intecsoft.demo.fbdemo.mappers.GutachterMapper;
import de.intecsoft.demo.fbdemo.model.Gutachter;
import de.intecsoft.demo.fbdemo.mappers.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GutachterSyncService {
    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private GutachterMapper gutachterMapper;

    public void sync() {
        gutachterMapper.clear();
        List<Gutachter> gutachterList = personMapper.getAllGutachter();
        for (Gutachter gutachter : gutachterList) {
            gutachterMapper.insert(gutachter);
        }
    }

    public List<Gutachter> load() {
        return gutachterMapper.loadAll();
    }
}
