package de.intecsoft.demo.fbdemo.config;

import de.intecsoft.demo.fbdemo.mappers.CityMapper;
import de.intecsoft.demo.fbdemo.mappers.GutachterMapper;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@MapperScan("de.intecsoft.demo.fbdemo.firebird.mapper")
@Configuration
public class FirebirdMapperConfiguration {
    @Autowired
    private SqlSessionFactory firebirdSqlSessionFactory;

    @Bean(name = "cityMapper")
    public MapperFactoryBean<CityMapper> cityMapper() throws Exception {
        MapperFactoryBean<CityMapper> factory = new MapperFactoryBean<>(CityMapper.class);
        factory.setSqlSessionFactory(firebirdSqlSessionFactory);
        return factory;
    }

    @Bean(name = "gutachterMapper")
    public MapperFactoryBean<GutachterMapper> gutachterMapper() throws Exception {
        MapperFactoryBean<GutachterMapper> factory = new MapperFactoryBean<>(GutachterMapper.class);
        factory.setSqlSessionFactory(firebirdSqlSessionFactory);
        return factory;
    }
}
