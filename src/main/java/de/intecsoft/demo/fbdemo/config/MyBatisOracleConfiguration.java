package de.intecsoft.demo.fbdemo.config;

import de.intecsoft.demo.fbdemo.mappers.PersonMapper;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.boot.autoconfigure.SpringBootVFS;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class MyBatisOracleConfiguration {
    @Value("${oracle.dataSource.url}")
    private String url;

    @Value("${oracle.dataSource.username}")
    private String username;

    @Value("${oracle.dataSource.password}")
    private String password;

    @Value("${oracle.dataSource.driverClassName}")
    private String driver;

    @Bean
    public SqlSessionFactory oracleSqlSessionFactory() throws Exception {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = resolver.getResources("classpath*:de/intecsoft/demo/fbdemo/mappers/**/*.xml");

        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(oracleDataSource());
        factoryBean.setVfs(SpringBootVFS.class);
        factoryBean.setMapperLocations(resources);
        return factoryBean.getObject();
    }

    @Bean
    public SqlSessionTemplate oracleSqlSession() throws Exception {
        return new SqlSessionTemplate(oracleSqlSessionFactory());
    }

    @Bean
    public DataSource oracleDataSource() throws Exception {
        DriverManagerDataSource source = new DriverManagerDataSource();
        source.setDriverClassName(driver);
        source.setUrl(url);
        source.setUsername(username);
        source.setPassword(password);
        return source;
    }

    @Bean
    public DataSourceTransactionManager oracleTransactionManager() throws Exception {
        return new DataSourceTransactionManager(oracleDataSource());
    }

    @Bean(name = "PersonMapper")
    public MapperFactoryBean<PersonMapper> cityMapper() throws Exception {
        MapperFactoryBean<PersonMapper> factory = new MapperFactoryBean<PersonMapper>(PersonMapper.class);
        factory.setSqlSessionFactory(oracleSqlSessionFactory());
        return factory;
    }
}