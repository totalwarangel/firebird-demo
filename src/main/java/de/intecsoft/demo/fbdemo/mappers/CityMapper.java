package de.intecsoft.demo.fbdemo.mappers;

import de.intecsoft.demo.fbdemo.model.City;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CityMapper {
    //@Select("SELECT * FROM CITY WHERE state = #{state}")
    List<City> findByState(@Param("state") String state);
}
