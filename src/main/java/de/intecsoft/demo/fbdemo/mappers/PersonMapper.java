package de.intecsoft.demo.fbdemo.mappers;

import de.intecsoft.demo.fbdemo.model.Gutachter;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PersonMapper {
    List<Gutachter> getAllGutachter();
}