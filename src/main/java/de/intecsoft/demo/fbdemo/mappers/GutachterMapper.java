package de.intecsoft.demo.fbdemo.mappers;

import de.intecsoft.demo.fbdemo.model.Gutachter;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GutachterMapper {
    List<Gutachter> loadAll();

    Gutachter load(Long id);

    void insert(Gutachter gutachter);

    void update(Gutachter gutachter);

    void delete(Long id);

    void clear();
}