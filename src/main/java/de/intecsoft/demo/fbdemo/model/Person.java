package de.intecsoft.demo.fbdemo.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Krella, Christian (christian.krella@intecsoft.de)
 */
public class Person implements Serializable {

    private Long id;

    private String typ;

    private String name;

    private String vorname;

    private String namenszusatz;

    private String vorsatzwort;

    private Date geburtsdatum;

    private String krankenversichertennummer;

    private String bemerkung;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayText() {
        return getName() + ", " + getVorname();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNamenszusatz() {
        return namenszusatz;
    }

    public void setNamenszusatz(String namenszusatz) {
        this.namenszusatz = namenszusatz;
    }

    public String getVorsatzwort() {
        return vorsatzwort;
    }

    public void setVorsatzwort(String vorsatzwort) {
        this.vorsatzwort = vorsatzwort;
    }

    public Date getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public String getKrankenversichertennummer() {
        return krankenversichertennummer;
    }

    public void setKrankenversichertennummer(String krankenversichertennummer) {
        this.krankenversichertennummer = krankenversichertennummer;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }
}
