package de.intecsoft.demo.fbdemo.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class City implements Serializable {
    private Long id;
    private String city;
    private String state;
}
