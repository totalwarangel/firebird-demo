package de.intecsoft.demo.fbdemo;

import de.intecsoft.demo.fbdemo.model.Gutachter;
import de.intecsoft.demo.fbdemo.service.GutachterSyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class FirebirdDemoApplication implements CommandLineRunner {
	@Autowired
	private GutachterSyncService syncService;

	public FirebirdDemoApplication() {}

	public static void main(String[] args) {
		SpringApplication.run(FirebirdDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		syncService.sync();
		List<Gutachter> newGutachterList = syncService.load();

		System.out.println("--------------------");
		System.out.println("Gutachter:");
		for (Gutachter g : newGutachterList) {
			System.out.println(g.getVorname() + " " + g.getName());
		}
		System.out.println("--------------------");
	}
}
