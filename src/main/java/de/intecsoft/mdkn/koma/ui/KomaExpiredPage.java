package de.intecsoft.mdkn.koma.ui;

import de.intecsoft.xpert.core.XpertCoreApplication;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.request.http.WebResponse;
import org.apache.wicket.request.resource.PackageResourceReference;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Krella, Christian (christian.krella@intecsoft.de)
 */
public class KomaExpiredPage extends WebPage {

    public KomaExpiredPage() {
        super();
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        setStatelessHint(true);
        setVersioned(false);

        add(new BookmarkablePageLink<Void>("homePage", getApplication().getHomePage()) {
            @Override
            protected boolean getStatelessHint() {
                return true;
            }
        });
    }

    public void renderHead(IHeaderResponse response) {
        response.render(CssHeaderItem.forReference(new PackageResourceReference(XpertCoreApplication.class, "resources/css/core.css")));
    }

    @Override
    protected void setHeaders(final WebResponse response) {
        response.setStatus(HttpServletResponse.SC_GONE);
    }

    @Override
    public boolean isErrorPage() {
        return true;
    }
}
