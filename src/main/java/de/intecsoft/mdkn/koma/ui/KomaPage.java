package de.intecsoft.mdkn.koma.ui;

import com.giffing.wicket.spring.boot.context.scan.WicketHomePage;
import de.intecsoft.xpert.core.XpertCorePage;
import de.intecsoft.xpert.core.data.SystemContext;
import org.apache.wicket.markup.html.WebPage;
import org.wicketstuff.annotation.mount.MountPath;

public abstract class KomaPage extends XpertCorePage {

    @Override
    public void onPageLeft() {

    }

    @Override
    protected String getSystemName() {
        return "xpert-dummy";
    }

    @Override
    protected boolean isMenueWmcVisible() {
        return true;
    }

    @Override
    protected boolean hasOneOfRoles(String... strings) {
        return false;
    }

    @Override
    protected String getUserName() {
        return "mhr";
    }

    @Override
    protected String getStruktureinheit() {
        return "GKVA";
    }

    @Override
    protected SystemContext getSystemContext() {
        return SystemContext.XPERT_KOMA;
    }
}
