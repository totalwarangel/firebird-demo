package de.intecsoft.mdkn.koma.ui;

import de.intecsoft.xpert.core.XpertErrorPanel;

public class KomaErrorPage extends KomaPage {
    public KomaErrorPage(Exception exception) {
        super();

        XpertErrorPanel panel = new XpertErrorPanel("errorPanel", exception) {
            @Override
            protected boolean allowViewStacktrace() {
                return true;
            }
        };
        add(panel);
    }
}
