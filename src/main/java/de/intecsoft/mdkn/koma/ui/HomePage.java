package de.intecsoft.mdkn.koma.ui;

import com.giffing.wicket.spring.boot.context.scan.WicketHomePage;
import org.wicketstuff.annotation.mount.MountPath;

@MountPath("home")
@WicketHomePage
public class HomePage extends KomaPage {

    public HomePage() {
        initPage();
    }

    private void initPage() {

    }
}
