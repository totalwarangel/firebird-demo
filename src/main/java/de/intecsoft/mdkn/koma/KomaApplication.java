package de.intecsoft.mdkn.koma;

import com.giffing.wicket.spring.boot.starter.app.WicketBootStandardWebApplication;
import de.intecsoft.mdkn.koma.ui.KomaErrorPage;
import de.intecsoft.mdkn.koma.ui.KomaExpiredPage;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.markup.html.IPackageResourceGuard;
import org.apache.wicket.markup.html.SecurePackageResourceGuard;
import org.apache.wicket.protocol.http.PageExpiredException;
import org.apache.wicket.request.IRequestCycle;
import org.apache.wicket.request.IRequestHandler;
import org.apache.wicket.request.component.IRequestablePage;
import org.apache.wicket.request.cycle.IRequestCycleListener;
import org.apache.wicket.request.cycle.PageRequestHandlerTracker;
import org.apache.wicket.request.cycle.RequestCycle;
import org.springframework.stereotype.Component;

/**
 * Application configuration
 */
@Component
public class KomaApplication extends WicketBootStandardWebApplication {
    @Override
    protected void init() {
        super.init();

        //getApplicationSettings().setInternalErrorPage();

        IPackageResourceGuard packageResourceGuard = getResourceSettings().getPackageResourceGuard();
        if (packageResourceGuard instanceof SecurePackageResourceGuard) {
            SecurePackageResourceGuard guard = (SecurePackageResourceGuard) packageResourceGuard;
            guard.addPattern("+*.json");
        }

        setSpecialErrorPage();
    }

    private void setSpecialErrorPage() {
        getRequestCycleListeners().add(new IRequestCycleListener() {
            @Override
            public IRequestHandler onException(final RequestCycle cycle, final Exception ex) {
                return new IRequestHandler() {
                    @Override
                    public void respond(IRequestCycle iRequestCycle) {
                        //Gibt die Page zurueck wo die Exception geworfen wurde
                        IRequestablePage page = null;
                        if (null != PageRequestHandlerTracker.getLastHandler(RequestCycle.get()) &&
                                PageRequestHandlerTracker.getLastHandler(RequestCycle.get()).isPageInstanceCreated()) {
                            page = PageRequestHandlerTracker.getLastHandler(RequestCycle.get()).getPage();
                        }

                        if (ex instanceof PageExpiredException) {
                            if (RuntimeConfigurationType.DEPLOYMENT.equals(getConfigurationType())) {
                                ((RequestCycle) iRequestCycle).setResponsePage(KomaExpiredPage.class);
                            }
                        }
                        else {
                            ((RequestCycle) iRequestCycle).setResponsePage(new KomaErrorPage(ex));
                        }
                    }

                    @Override
                    public void detach(IRequestCycle iRequestCycle) {
                        // TODO
                    }
                };
            }
        });
    }
}