package de.intecsoft.mdkn.koma.model;

import de.intecsoft.xpert.core.provider.Identifiable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Table: PFLEGEEINRICHTUNG
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pflegeeinrichtung implements Serializable, Identifiable {
    protected Long id;

    protected Long ikz;

    protected String h_name1;

    protected String h_name2;

    protected String h_name3;
}
