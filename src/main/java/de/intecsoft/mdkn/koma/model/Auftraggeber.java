package de.intecsoft.mdkn.koma.model;

import de.intecsoft.xpert.core.provider.Identifiable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Table AUFTRAGGEBER
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Auftraggeber implements Serializable, Identifiable {
    protected Long id;

    protected Long ikz;

    protected String name1;

    protected String name2;
}
