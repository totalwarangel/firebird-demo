package de.intecsoft.mdkn.koma.model;

import de.intecsoft.xpert.core.provider.Identifiable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Table: AUFTRAG
 */
@Getter
@Setter
@EqualsAndHashCode()
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Auftrag implements Serializable, Identifiable {
    protected Long id;

    @Setter(AccessLevel.NONE)
    protected final String anlass = "600";

    protected Long auftraggeberId;

    protected Long productId;

    protected Long statusId;

    /* FIXME: Whatever that is... */
    protected String name;
}
