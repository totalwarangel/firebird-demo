package de.intecsoft.mdkn.koma.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.boot.autoconfigure.SpringBootVFS;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class MyBatisFirebirdConfiguration {
    @Value("${firebird.dataSource.url}")
    private String url;

    @Value("${firebird.dataSource.username}")
    private String username;

    @Value("${firebird.dataSource.password}")
    private String password;

    @Value("${oracle.dataSource.driverClassName}")
    private String driver;

    @Bean(name = "firebirdSqlSessionFactory")
    public SqlSessionFactory firebirdSqlSessionFactory() throws Exception {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = resolver.getResources("classpath*:de/intecsoft/mdkn/koma/mappers/**/*.xml");

        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(firebirdDataSource());
        factoryBean.setVfs(SpringBootVFS.class);
        factoryBean.setMapperLocations(resources);
        return factoryBean.getObject();
    }

    @Bean(name = "firebirdSqlSession")
    public SqlSessionTemplate firebirdSqlSession() throws Exception {
        return new SqlSessionTemplate(firebirdSqlSessionFactory());
    }

    @Bean(name = "firebirdDataSource")
    public DataSource firebirdDataSource() throws Exception {
        DriverManagerDataSource source = new DriverManagerDataSource();
        source.setDriverClassName(driver);
        source.setUrl(url);
        source.setUsername(username);
        source.setPassword(password);
        return source;
    }

    @Bean(name = "firebirdTransactionManager")
    public DataSourceTransactionManager firebirdTransactionManager() throws Exception {
        return new DataSourceTransactionManager(firebirdDataSource());
    }
}