package de.intecsoft.mdkn.koma.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperOracleConfiguration {
    @Autowired
    private SqlSessionFactory oracleSqlSessionFactory;
}