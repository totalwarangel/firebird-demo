package de.intecsoft.mdkn.koma.config;

import de.intecsoft.mdkn.koma.repository.qasp.auftrag.QaspAuftragDao;
import de.intecsoft.mdkn.koma.repository.qasp.auftrag.QaspAuftragMapper;
import de.intecsoft.mdkn.koma.repository.qasp.auftraggeber.QaspAuftraggeberDao;
import de.intecsoft.mdkn.koma.repository.qasp.auftraggeber.QaspAuftraggeberMapper;
import de.intecsoft.mdkn.koma.repository.qasp.pflegeeinrichtung.QaspPflegeeinrichtungDao;
import de.intecsoft.mdkn.koma.repository.qasp.pflegeeinrichtung.QaspPflegeeinrichtungMapper;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class for repository access to the firebird database used by QASP.
 */
@Configuration
public class DaoFirebirdConfiguration {
    @Autowired
    private SqlSessionFactory firebirdSqlSessionFactory;

    @Bean("pqmPflegeeinrichtungMapper")
    public MapperFactoryBean<QaspPflegeeinrichtungMapper> pqmPflegeeinrichtungMapper() {
        MapperFactoryBean<QaspPflegeeinrichtungMapper> factory = new MapperFactoryBean<>(QaspPflegeeinrichtungMapper.class);
        factory.setSqlSessionFactory(firebirdSqlSessionFactory);
        return factory;
    }

    @Bean("pqmPflegeeinrichtungDao")
    public QaspPflegeeinrichtungDao pqmPflegeeinrichtungDao() throws Exception {
        return new QaspPflegeeinrichtungDao(pqmPflegeeinrichtungMapper().getObject());
    }

    @Bean("pqmAuftraggeberMapper")
    public MapperFactoryBean<QaspAuftraggeberMapper> pqmAuftraggeberMapper() {
        MapperFactoryBean<QaspAuftraggeberMapper> factory = new MapperFactoryBean<>(QaspAuftraggeberMapper.class);
        factory.setSqlSessionFactory(firebirdSqlSessionFactory);
        return factory;
    }

    @Bean("pqmAuftraggeberDao")
    public QaspAuftraggeberDao pqmAuftraggeberDao() throws Exception {
        return new QaspAuftraggeberDao(pqmAuftraggeberMapper().getObject());
    }

    @Bean("pqmAuftragMapper")
    public MapperFactoryBean<QaspAuftragMapper> pqmAuftragMapper() {
        MapperFactoryBean<QaspAuftragMapper> factory = new MapperFactoryBean<>(QaspAuftragMapper.class);
        factory.setSqlSessionFactory(firebirdSqlSessionFactory);
        return factory;
    }

    @Bean("pqmAuftragDao")
    public QaspAuftragDao pqmAuftragDao() throws Exception {
        return new QaspAuftragDao(pqmAuftragMapper().getObject());
    }
}
