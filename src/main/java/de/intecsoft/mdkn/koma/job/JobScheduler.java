package de.intecsoft.mdkn.koma.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This Components triggers repetitive actions aka jobs.
 *
 * Cron notation generator:
 * @see <a href="https://riptutorial.com/spring/example/21209/cron-expression"></a>
 */
@Component
public class JobScheduler {
    /**
     * Scheduled at the start of each hour until eternity.
     * FIXME: This should be removed!
     */
    @Scheduled(cron = "0 0 0/1 * * *")
    public void exampleJob() {
        SimpleDateFormat format = new SimpleDateFormat("h");
        System.out.println("Liebe Leute lasst Euch sagen, die Uhr hat " + format.format(new Date()) + " geschlagen!");
    }
}
