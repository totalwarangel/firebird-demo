package de.intecsoft.mdkn.koma.repository.qasp.auftrag;

import de.intecsoft.mdkn.koma.model.Auftrag;
import de.intecsoft.mdkn.koma.repository.IMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface QaspAuftragMapper extends IMapper<Auftrag> {
    @Override
    Auftrag load(Long id);

    @Override
    List<Auftrag> loadAll();

    @Override
    void insert(Auftrag entry);

    @Override
    void update(Auftrag entry);

    @Override
    void delete(Long id);
}
