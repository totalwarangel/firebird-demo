package de.intecsoft.mdkn.koma.repository;

import java.io.Serializable;
import java.util.List;

/**
 * Basic mapper interface. This need to be extended by the actual used mappers.
 *
 * Remember, that you must implement this methods in any extending mappers and their XML!
 *
 * @param <T> Type of entry
 */
public interface IMapper<T extends Serializable> {
    T load(Long id);

    List<T> loadAll();

    void insert(T entry);

    void update(T entry);

    void delete(Long id);
}
