package de.intecsoft.mdkn.koma.repository.qasp.pflegeeinrichtung;

import de.intecsoft.mdkn.koma.repository.IMapper;
import de.intecsoft.mdkn.koma.model.Pflegeeinrichtung;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Mapper for a Pflegeeinrichtung on the side of QASP.
 */
@Mapper
public interface QaspPflegeeinrichtungMapper extends IMapper<Pflegeeinrichtung> {
    @Override
    Pflegeeinrichtung load(Long id);

    @Override
    List<Pflegeeinrichtung> loadAll();

    @Override
    void insert(Pflegeeinrichtung entry);

    @Override
    void update(Pflegeeinrichtung entry);

    @Override
    void delete(Long id);
}
