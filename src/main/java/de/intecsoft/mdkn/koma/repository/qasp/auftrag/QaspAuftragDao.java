package de.intecsoft.mdkn.koma.repository.qasp.auftrag;

import de.intecsoft.mdkn.koma.model.Auftrag;
import de.intecsoft.mdkn.koma.repository.AbstractDao;

/**
 * DAO to access the Auftrag in QASP.
 */
public class QaspAuftragDao extends AbstractDao<Auftrag, QaspAuftragMapper> {
    public QaspAuftragDao(QaspAuftragMapper mapper) {
        this.mapper = mapper;
    }
}
