package de.intecsoft.mdkn.koma.repository;

import de.intecsoft.xpert.core.provider.Identifiable;

import java.io.Serializable;
import java.util.List;

/**
 * Abstract DAO managing the standard CRUD operations.
 *
 * @param <T> Type of entry
 * @param <M> Type of mapper
 */
public abstract class AbstractDao<T extends Serializable & Identifiable, M extends IMapper<T>> implements IDao<T> {
    protected M mapper;

    @Override
    public T load(Long id) throws Exception {
        return mapper.load(id);
    }

    @Override
    public List<T> loadAll() throws Exception {
        return mapper.loadAll();
    }

    @Override
    public T merge(T entry) throws Exception {
        if (null == entry.getId()) {
            mapper.insert(entry);
        }
        else {
            mapper.update(entry);
        }
        return entry;
    }

    @Override
    public void delete(Long id) throws Exception {
        mapper.delete(id);
    }
}
