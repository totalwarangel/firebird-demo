package de.intecsoft.mdkn.koma.repository;

import java.io.Serializable;
import java.util.List;

/**
 * Interface for the basic dao.
 *
 * @param <T> Type of entry
 */
public interface IDao<T extends Serializable> {
    T load(Long id) throws Exception;

    List<T> loadAll() throws Exception;

    T merge(T entry) throws Exception;

    void delete(Long id) throws Exception;
}
