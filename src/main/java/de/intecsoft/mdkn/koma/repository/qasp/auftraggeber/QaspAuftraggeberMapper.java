package de.intecsoft.mdkn.koma.repository.qasp.auftraggeber;

import de.intecsoft.mdkn.koma.repository.IMapper;
import org.apache.ibatis.annotations.Mapper;
import de.intecsoft.mdkn.koma.model.Auftraggeber;

import java.util.List;

/**
 * Mapper for a Pflegeeinrichtung on the side of QASP.
 */
@Mapper
public interface QaspAuftraggeberMapper extends IMapper<Auftraggeber> {
    @Override
    Auftraggeber load(Long id);

    @Override
    List<Auftraggeber> loadAll();

    @Override
    void insert(Auftraggeber entry);

    @Override
    void update(Auftraggeber entry);

    @Override
    void delete(Long id);
}
