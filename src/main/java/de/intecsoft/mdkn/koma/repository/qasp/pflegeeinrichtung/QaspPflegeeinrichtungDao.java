package de.intecsoft.mdkn.koma.repository.qasp.pflegeeinrichtung;

import de.intecsoft.mdkn.koma.repository.AbstractDao;
import de.intecsoft.mdkn.koma.model.Pflegeeinrichtung;

/**
 * DAO to access the Pflegeinrichtung in QASP.
 */
public class QaspPflegeeinrichtungDao extends AbstractDao<Pflegeeinrichtung, QaspPflegeeinrichtungMapper> {
    public QaspPflegeeinrichtungDao(QaspPflegeeinrichtungMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public Pflegeeinrichtung merge(Pflegeeinrichtung entry) throws Exception {
        throw new Exception("Table is read only!");
    }

    @Override
    public void delete(Long id) throws Exception {
        throw new Exception("Table is read only!");
    }
}
