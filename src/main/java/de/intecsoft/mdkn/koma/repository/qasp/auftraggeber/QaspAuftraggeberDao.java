package de.intecsoft.mdkn.koma.repository.qasp.auftraggeber;

import de.intecsoft.mdkn.koma.repository.AbstractDao;
import de.intecsoft.mdkn.koma.model.Auftraggeber;

/**
 * DAO to access the Auftraggeber in QASP.
 */
public class QaspAuftraggeberDao extends AbstractDao<Auftraggeber, QaspAuftraggeberMapper> {
    public QaspAuftraggeberDao(QaspAuftraggeberMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public Auftraggeber merge(Auftraggeber entry) throws Exception {
        throw new Exception("Table is read only!");
    }

    @Override
    public void delete(Long id) throws Exception {
        throw new Exception("Table is read only!");
    }
}
