# Firebird & Oracle Demo

## Introduction

This application is a demo how to access both oracle and firebird database at the same time. Some Gutachter will be
fetched from the MDK db and put into a local firebird database. The data will be printet to stdout.

## Steps before run

Fulfill these requirements and the app should run:

* At least Firebird 2.5 installed on your system
* DB Manager for Firebird to check up the data (f.e "Flamerobin")
* Create a firebird db with the credentials from application.properties
* Check (and adjust) credentials for the MDK db.