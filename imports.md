# Xpert <> QASP

Welche Daten aus dem Xpert wie in QASP reinkommen...

#### Fragen

* Wie und wo werden die Daten (Auftrag und Co.) verknüpft?
* [Auftrag] Eingangsdatum wird gesetzt / nicht gesetzt?
* [Auftrag] Was ist *AUFTRAGSTEXT*?
* [Institution] Was passiert, wenn es die IK-Nummer nicht in QASP gibt?
* [Träger] Verbindung zu *KOSTENTRÄGER*-Tabelle?
* [Träger] Warum gibt es keine IKZ?
* [Auftragsklassifizierung] Wo soll das hin?
* [Auftragsklassifizierung] Regelprüfung?
* [Auftragsklassifizierung] Regelart?

#### Auftrag (*AUFTRAG*)

* *ID* -> Xpert Auftragsnummer \
  Typ: Long
* ANLASS ist fix (600 ?)
* Auftraggeber ID anhand IK-Nummer
* Eingangsdatum

#### Pflegeeinrichtung (*PFLEGEEINRICHTUNG*)

Die Pflegeeinrichtung wird über IKZ identifiziert. \
In Xpert ist das die IK-Nummer.

Spalten:
* __Name1, Name2, Name3__
* *IKZ* -> IK Nummer \
  Typ: Long \
  Wenn IKZ nicht in QASP vorhanden:
* *H_STRASSE* -> PE Straße u. Hausnummer \
  Typ: String
* *H_PLZ_ID* -> PE PLZ
* *H_ORT* -> PE Ort \
  Typ: String
* *PE Telefon* -> PE Telefon \
  Typ: String
* *H_TELEFAX* -> PE Fax \
  Typ: String
* *H_EMAIL* -> PE Email \
  Typ: String
* __*H_INTERNET* -> ?__
  
#### Auftraggeber (AUFTRAGGEBER)

Der Auftraggeber wird über IKZ identifiziert. \
In Xpert ist das die IK-Nummer.

* NDS-Auftraggeber -> Institution \
* __*NAME1, NAME2* ?__
* *IKID* -> *ID*
* __*STRASSE, PLZ_ID, TELEFON1, TELEFON2, FAX, IKZ* ?__

#### Träger (TRAEGER)

* __*ID* ?__
* *T_NAME1* -> Inst. Name \
  Typ: String
* *T_STRASSE* -> PE Straße u. Hausnummer \
  Typ: String
* *T_ORT, T_PLZ_ID* -> PE PLZ u. Ort \
  Typ: String
* *T_TELEFON* -> PE Telefon \
  Typ: String
* *T_TELEFAX* -> PE Fax \
  Typ: String
  
#### Auftragsklassifizuerung

Wo soll das hin?

* Regelprüfung -> ? \
  Typ: String
* Prüfungsart -> ? \
  Typ: ?
* Termine (Gutachter, Tag, Datum)
* Uhrzeit der Prüfung
* Anzahl Plätze
* Gutachter (Name, Vorname, Typ) \
  GT -> HG, NG, EG
* Datum Vorprüfung \
  Typ: Date
* Abschlussdatum Versorgungsvertrag \
  Typ: Date